package org.apache.commons.collections;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by FireFire on 5/12 0012.
 */
public class SetUtilsTest {
    private Set<String> sets = new HashSet<String>();

    @BeforeTest
    public void setup(){
        sets.add("hello");
        sets.add("horld");
        sets.add("what");
        sets.add("yyy");
    }

    @Test
    public void testPredicatedSet(){
        int matches = CollectionUtils.countMatches(sets, new Predicate() {
            public boolean evaluate(Object o) {
                if (!o.toString().contains("h"))
                    return false;
                return true;
            }
        });

        Assert.assertTrue(matches==3);
    }
}
